---
title: "Websites you can get lost in"
date: 2024-04-04T15:40:24+06:00
# talks thumb
image : "images/blogs/blog3.jpg"
draft: false
# description
description: "blog web culture"
---

### Blog web culture

I love web sites I can get lost in. Densely woven orbs of information. I mean specifically personal websites. Snippets of someone's life, or thoughts, They're not always Wiki's but that does seem to be the most straightforward way to create such dense bundles of a website. I hope one day, my own website can become like that.

I wanna make my own minimal bloat website on my own, and this page could be the one. This page is for professional one and i have another one like a indieweb only for my blog and i dont wanna make it open as my thoughts are more diverse and i dont want to make people judge me.
