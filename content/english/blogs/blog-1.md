---
title: "Why Self Hosting Matters"
date: 2023-08-29T15:40:24+06:00
# talks thumb
image : "images/blogs/blog1.jpg"
draft: false
# description
description: "This is meta description"
---

# Why Privacy Matters?

In today's digital age, the concept of self-hosting and open-source technology has taken on an ever-increasing significance. It's not just about technology; it's about reclaiming control over your data and your privacy. As we move on this journey, it becomes evident that safeguarding ourselves, our families, and our friends from the reach of big tech, governmental agencies, and any other entities encroaching upon our right to privacy is of utmost importance.

Our online activities have turned us into commodities, with our every move being closely tracked and analyzed. Our data, ranging from personal details and email correspondences to social media posts and location information, is being exploited behind the scenes, often without our consent.

<!-- ![Social Dilemma](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/p05_01.webp) -->

## The Value of Privacy

One often I hear these arguments from my friends, "I have nothing to hide, so why should I care about privacy?" This line of thinking is both flawed and risky. It's crucial to understand that personal convenience offered by proprietary services can come at the cost of compromising your own privacy.

For instance, proprietary platforms like Google or Meta might seem convenient and free, but in reality, they offer cost-free usage, not true freedom. Users of such platforms inadvertently become products, with their data being monetized and exploited for profit.

## Session on Self-hosting

Our recent interactive and informative session on self-hosting provided our peers with an insightful journey into the world of personal data ownership. We deployed various services using our local Linux PCs and explored the potential of Docker containers.

The session was handled by Ragulkanth, who covered the fundamentals of the internet and introduced the concept of mesh networks. This laid the groundwork for understanding how data travels across the internet, shining a light on routers, ISPs, and the TCP/IP protocol suite.

Furthermore, we jumped into the concept of mesh networks, discovering their decentralized and self-organizing nature. Mesh networks operate independently of a centralized infrastructure, enabling devices to communicate directly with one another.

During the session, we successfully deployed several exciting self-hosted services:

- **Bitwarden:** We set up Bitwarden, a secure password manager that helps us safeguard our credentials.
- **Jellyfin:** This media server allows us to seamlessly organize and stream our favorite movies and TV shows.
- **Pi-hole:** A potent ad-blocking solution that enhances browsing experiences by effectively blocking ads network-wide.
- **Nextcloud:** We explored Nextcloud, a comprehensive self-hosted cloud storage solution, enabling secure file synchronization and sharing.

<!-- ![SessionPic](https://gitlab.com/sotc/sotc.gitlab.io/-/raw/main/assets/images/media/p05_02.webp) -->
We(SOTC) extend our heartfelt gratitude to each participant who joined us for the self-hosting session. Seeing so many of students showing up and engage in the session brought immense joy to our SOTC.