---
title: "Rant on Social Media"
date: 2024-03-30T15:40:24+06:00
# talks thumb
image : "images/blogs/blog2.avif"
draft: false
# description
description: "This is meta description"
---

## "Keep that phone aside" - My mom was always right.

My mom always hated social media. I always made fun of her because she was always late to all the news. Her main reason against social media was "why would I want everyone to know what I'm doing? And why should I care what they are doing?". I didn't understand at the time, but now I do.

I remember when I was 14 years old social media started ramping up. I created a Instagram account to go with the flow. Instagram had a feed with all your friends' posts, so there was no need to visit their profile just to see what they were doing. It sounded very cool at the moment! I won't lie, I liked Insta a lot, sharing my music, posting funny pictures, profiles, stories, following my friends to see their travels, following superstars to see their perfect lifes. It all seemed pretty cool and innovative.

Stop.

Just stop.

Social media has become too overwhelming in the last couple of years. People are lonelier and more depressed because of social media. The [FOMO (Fear of missing out)](https://en.wikipedia.org/wiki/Fear_of_missing_out) is at an all-time high.

Now that I know all of this, **why would I want to be part of something that could make me feel anxiety, depression and have self-esteem issues?** That was the question I made to myself around 6 months ago. I consider myself an exaggerated person, so I went full in. The goal was to stop using **all** social media for 1 month and see the results. So here are my thoughts about the experiment

### I feel more relaxed

I don't have the need to open my phone when I'm at a bus stop or just doing nothing. I don't care what my friends are posting, I don't care if an influencer bought something or traveled to someplace. Before leaving social media, those things had little impact on me and my daily life, so why should I care?

### I have more free time, or time to do more productive stuff

The first week I realized how much time I was wasting using social media, I was wasting between avg 3 to 4 hours a day in social media, but now I have that time for myself, updated my website, updated and improved my working PC and many other things.


### My "reach to my pocket for my phone" tick stopped

I wasn't checking my phone as much as before. I could meet and talk to people without my phone being on the table, and I also realized how rude it is to be ignored because everyone at the table is checking Instagram on their phones.


### Conclusions?

My mom was right, as always. The information overload was fun at first, but then it became _overwhelming_. Social media has entered our society as a spy and made us dependent on it, and that's bad. We need to get rid of it.

This experiment started in August 2023 and I can happily say that I left social media for 1 month and after months I went back to check upon my friends, memes and reels that they shared me. I use Reddit / LinkedIn because I sometimes use it for work.

Guess what?? Once again decided to close all my social media for a month and planned to track my process everyday for straight one month.

I now recommend everyone I know to shut down everything for a while and see how it feels. They might find out that there is a big world out there if they just move their head up and away from their phones.
