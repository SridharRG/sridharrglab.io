---
title: "Just starting writing"
date: 2020-03-14T15:40:24+06:00
# talks thumb
image : "images/blogs/blog6.webp"
draft: false
# description
description: "This is meta description"
---


**How I feel when i write a blog**

When I write about something, the vague thoughts I have about that topic becomes more concrete. It helps me understand it in newer ways. Similarly, once I write something down and give a URL to it, it becomes a valuable piece of content on the internet that I can later share to others for reference.

If you have a blog, write a post. 
If you do not have a blog, start a blog, and write a post. 

