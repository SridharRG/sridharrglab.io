---
title: "Bus Ticket Booking in Java"
date: 2020-03-14T15:40:24+06:00
# Project thumb
image : "images/projects/project1.jpg"
draft: false
# description
description: "Bus ticket reservation system using javaFX - Deployment"
---

I undertook a project titled "Bus Ticket Reservation System" serving as my final year project in my last UG days.

The project's frontend was developed using Scene Builder, a user-friendly tool renowned for its drag-and-drop interface, resulting in .fxml files. Connecting this frontend to the backend logic was done through the controller file. Leveraging JavaFX, I seamlessly integrated the backend functionalities.

For the database aspect, MySQL was chosen due to my familiarity with it at the time. I didn't step ahead into exploring alternative database stacks, my bad. The connectivity between the application and the MySQL database was facilitated through JDBC, encapsulated within the ConnectionClass.java file.

To elaborate on the project's content(as should I do, no other go), I meticulously compiled a project-report.pdf. I utilized AI-generated narratives for detailed insights(nvm). The report is  available in my GitHub repo for further exploration.

 [Have a look into my shitty project](https://github.com/SridharRG/busticket-booking-java).